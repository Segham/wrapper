export enum FontFlags_t {
	NONE = 0,
	UNDERLINE = 1 << 0,
	STRIKEOUT = 1 << 1,
	SYMBOL = 1 << 2,
	GAUSSIANBLUR = 1 << 3,
	ROTARY = 1 << 4,
	DROPSHADOW = 1 << 5,
	ADDITIVE = 1 << 6,
	OUTLINE = 1 << 7,
}
