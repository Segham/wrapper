import { WrapperClass } from "../../Decorators"
import WardObserver from "./WardObserver"

@WrapperClass("CDOTA_NPC_Observer_Ward_TrueSight")
export default class WardTrueSight extends WardObserver {
}
